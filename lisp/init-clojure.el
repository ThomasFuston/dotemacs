;;; init-clojure.el --- Clojure -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; Setup Emacs for development in Clojure.

;;; Code:

;; Flycheck clj-kondo module
(use-package flycheck-clj-kondo)

;; Major Treesitter mode for clojure
(use-package clojure-ts-mode
  :config
  (require 'flycheck-clj-kondo))

;; REPL development
(use-package cider
  :init
  (progn
    (add-hook 'clojure-mode-hook 'cider-mode)
    (add-hook 'clojurescript-mode-hook 'cider-mode)
    (add-hook 'clojurec-mode-hook 'cider-mode)
    (add-hook 'cider-repl-mode-hook 'cider-mode))
  :config
  (setq cider-repl-display-help-banner nil)
  (setq cider-auto-mode nil))

;; Better delimiters readability
(use-package rainbow-delimiters
  :defer t
  :init
  :hook
  (clojure-ts-mode . rainbow-delimiters-mode))



(provide 'init-clojure)
;;; init-clojure.el ends here
