;;; init-smart-parens.el --- Automagical pairs  -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; Automagical handle pairs of parens.

;;; Code:

(use-package smartparens
  :demand t
  :hook (prog-mode . smartparens-mode)
  :diminish
  :config
  (require 'smartparens-config)
  (smartparens-global-mode +1)
  (show-smartparens-global-mode +1)
  ;; Stop pairing single quotes in elisp
  (sp-local-pair 'emacs-lisp-mode "'" nil :actions nil))



(provide 'init-smart-parens)
;;; init-smart-parens.el ends here
