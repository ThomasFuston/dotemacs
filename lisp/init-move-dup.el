;;; init-move-dup.el --- Move up and down -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; Add functionality to move lines and selections up and down line by line.

;;; Code:

(use-package move-dup
  :diminish
  :hook (after-init . global-move-dup-mode)
  :bind
  ("M-<up>" . md/move-lines-up)
  ("M-<down>" . md/move-lines-down))


(provide 'init-move-dup)
;;; init-move-dup.el ends here
