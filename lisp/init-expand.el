;;; init-text.el --- region selection -*- lexical-binding: t; -*-

;;; Commentary:
;;
;;  This will add syntactical region selection

;;; Code:

(use-package expand-region
  :bind ("M-i" . er/expand-region))



(provide 'init-expand)
;;; init-expand.el ends here
