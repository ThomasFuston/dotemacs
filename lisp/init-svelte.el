;;; init-svelte.el --- Svelte -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; Setup everything for a nice Svelte dev environment

;;; Code:

;; First derive a svelte-mode from web-mode
(define-derived-mode svelte-mode web-mode "Svelte"
  "Major mode for editing Svelte files.")

;; Second step load svelte mode on .svelte files.
(use-package svelte-mode
  :ensure nil
  :mode ("\\.svelte\\'" . svelte-mode)
  :after web-mode
  :config
  (provide 'svelte-mode))



(provide 'init-svelte)
;;; init-svelte.el ends here
