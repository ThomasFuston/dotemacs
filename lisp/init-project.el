;;; init-project.el --- Project mangement -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; Use the build in project.el and add keybindings

;;; Code:
(use-package project
  :ensure nil
  :diminish
  :chords
  ("jj" . hydra-projects/body)
  :hydra (hydra-projects (:color blue)
                         "Project Management"
                         ("f" project-find-file "file")
                         ("d" project-find-dir "directory")
                         ("b" project-switch-to-buffer "buffer")
                         ("c" project-find-regexp "code")
                         ("p" project-switch-project "project"))
  )



(provide 'init-project)
;;; init-project.el ends here
