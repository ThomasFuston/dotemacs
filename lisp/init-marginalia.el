;;; init-marginalia.el --- Margenalia -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; Marginalia is adding helpful 'annotations' of the lists presented
;; in the minibuffer by vertico.

;;; Code:

;; Add annotations
(use-package marginalia
  :demand t
  :after vertico
  :custom
  (marginalia-align 'right)             ; Alignment of information
  (marginalia-max-relative-age 0)
  :init
  (marginalia-mode))

;; Add icons
(use-package all-the-icons-completion
  :demand t
  :after (marginalia all-the-icons)
  :hook (marginalia-mode . all-the-icons-completion-marginalia-setup)
  :init
  (all-the-icons-completion-mode))



(provide 'init-marginalia)
;;; init-marginalia.el ends here
