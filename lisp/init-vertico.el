;;; init-vertico.el --- Vertico -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; Change the minibuffer interaction interface to
;; a improved and vertical one.

;;; Code:

(use-package vertico
  :demand t
  :custom
  (vertico-count 13)                    ; Show max 13 candidates at same time
  (vertico-cycle t)                     ; Go from last to first candidate
  (vertico-resize t)                    ; Do Resize the Minibuffer
  :init
  (vertico-mode)                        ; Enable vertico
  (vertico-reverse-mode))               ; Vertico reversed UI (mainly like Selectrum)



(provide 'init-vertico)
;;; init-vertico.el ends here
