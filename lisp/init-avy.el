;;; init-avy.el --- Fast navigation -*- lexical-binding: t -*-

;;; Commentary:
;;
;; Add functionality to lightning fast move trough
;; documents.

;;; Code:

(use-package avy
  :demand
  :chords
  (("jl" . avy-goto-line)
   ("jk" . avy-goto-char))
  :init
  (setq avy-all-windows nil
        avy-all-windows-alt t))


(provide 'init-avy)
;;; init-avy.el ends here
