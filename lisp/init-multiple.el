;;; init-multiple.el --- Multiple cursors -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; Adding multiple cursors function.

;;; Code:

(use-package multiple-cursors
  :demand
  :bind
  ("M-m" . mc/mark-next-like-this)
  ("M-M" . mc/mark-previous-like-this))


(provide 'init-multiple)
;;; init-multiple.el ends here
