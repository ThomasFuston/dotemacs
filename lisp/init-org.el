;;; init-org.el --- Org mode  -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; Setup org-mode and org-mode for Notetaking.

;;; Code:
;;
;; Required for this module on Linux - gcc & clang

(use-package org
  :config
  (custom-set-faces
   '(org-level-1 ((t (:inherit outline-1 :height 1.5))))
   '(org-level-2 ((t (:inherit outline-2 :height 1.3))))
   '(org-level-3 ((t (:inherit outline-3 :height 1.1))))
   '(org-level-4 ((t (:inherit outline-4 :height 1.0))))
   '(org-level-5 ((t (:inherit outline-5 :height 1.0))))
   (set-face-attribute 'org-document-title nil :height 2.0 :underline nil))
  )

(use-package org-roam
  :demand
  :custom
  (org-roam-directory "~/Nextcloud/Notizen/")
  (org-hide-emphasis-markers t)
  (org-src-fontify-natively t)
  (org-startup-indented t)
  (org-fontify-whole-heading-line t)
  (org-fontify-done-headline t)
  (org-pretty-entities t)
  (org-src-tab-acts-natively t)
  :bind ("<f12>" . hydra-org-roam/body)
  :config
  (org-roam-setup)
  ;; Hydra keybindings
  (defhydra hydra-org-roam (:color blue)
    "Org Roam"
    ("d" org-roam-dailies-capture-today "Quick Note today")
    ("t" org-roam-dailies-goto-today "Todays quicknote")
    ("i" org-roam-node-insert "insert link")
    ("f" org-roam-node-find "find org-roam files")))



(provide 'init-org)
;;; init-org.el ends here
