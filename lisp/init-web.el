;;; init-web.el --- Web templates -*- lexical-binding: t -*-

;;; Commentary:
;;
;; Setup webdevelopment environment for html/css

;;; Code:

(use-package web-mode
  :demand
  :init
  (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.pt?\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.css\\'" . web-mode))
  :custom
  (css-indent-offset 2)
  (web-mode-markup-indent-offset 2)
  (web-mode-css-indent-offset 2)
  (web-mode-code-indent-offset 2)
  (web-mode-indent-style 2))

;; Emmet mode
(use-package emmet-mode
  :diminish emmet-mode
  :hook (web-mode . emmet-mode)
  :init
  (setq emmet-move-cursor-between-quotes t))

;; SASS support
(use-package sass-mode)

;; SCSS support
(use-package scss-mode
  :init (setq-default scss-compile-at-save nil))

;; LESS support
(use-package less-css-mode)

;; YAML support
(use-package yaml-mode)

;; Simple server
(use-package simple-httpd)



(provide 'init-web)
;;; init-web.el ends here
