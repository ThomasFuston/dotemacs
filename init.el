;;; init.el --- Load my Emacs configuration -*- lexical-binding: t; no-byte-compile: t;-*-

;;; Commentary:
;;
;; This is the ever changing personal Emacs configuration of
;; Thomas Fuston, it is one of many such configuration, but
;; this one is mine.

;;; Code:
;;
;; Dependencies for full functionality:
;; ---
;; - gcc
;; - clang


;; Bootstrapping
;; -------------

;; Put settings set with the build-in customization interface
;; away into a seperate file.
(setq custom-file (locate-user-emacs-file "etc/custom.el"))
(when (file-exists-p custom-file) (load custom-file))

;; Load Paths
(add-to-list 'load-path (expand-file-name "lisp/" user-emacs-directory))
(add-to-list 'load-path (expand-file-name "defuns/" user-emacs-directory))

;; Personal information
(setq user-full-name "Thomas Fuston"
      user-mail-adress "thomasfuston@posteo.de")


;; Load configurations
;; -------------------

;; Core configs
(require 'init-package)			 ; Package management
(require 'init-ui)           ; User interface
(require 'init-base)         ; Baseline defaults

;; General functionality
(require 'init-vertico)      ; Vertical minibuffer interface
(require 'init-marginalia)   ; Add Annotaions
(require 'init-orderless)    ; Orderless completion style
(require 'init-corfu)        ; at-point autocompletion

(require 'init-crux)         ; Add helpful functionality
(require 'init-avy)          ; Move lightning fast trough files
(require 'init-multiple)     ; Multiple-Cursors
(require 'init-expand)       ; Expand region
(require 'init-move-dup)     ; Move selection/lines

(require 'init-undo)         ; Visualize Undohistory
(require 'init-eyebrowse)    ; Workspaces
(require 'init-project)      ; Project management

;; Coding general
(require 'init-smart-parens) ; Automagcally handle pairs of parens
(require 'init-indent)       ; Indentation
(require 'init-flycheck)     ; Syntax checker

;; Webdevelopment
(require 'init-web)          ; Web temaplates etc.
(require 'init-svelte)       ; Svelte Development
(require 'init-js)           ; Javascript
(require 'init-clojure)      ; Clojure


;; Notetaking
(require 'init-org)          ; Org-mode for Notetaking


;; Custom defuns
;; -------------

(require 'defun-jump-window) ;Jump Windows
(require 'defun-goto-line)   ;Improved goto-line


;; Last adjustments
;; ----------------

;; Last but not least - Reset garbage collection and revert
;; the file-name-handler
(add-hook 'emacs-startup-hook
          (lambda ()
            (setq gc-cons-threshold (* 20 1024 1024)
                  gc-cons-percentage 0.1)
            (setq file-name-handler-alist file-name-handler-alist-original)
            (makunbound 'file-name-handler-alist-original)))

;; Make sure Emacs get the right PATH from shell
(use-package exec-path-from-shell
  :init
  (exec-path-from-shell-initialize))



(provide 'init)
;;; init.el ends here
