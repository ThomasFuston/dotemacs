;;; early-init.el --- First file to load -*- lexical-binding: t; no-byte-compile: t; -*-
;;
;; Author: Thomas Fuston
;; URL: https://gitlab.com/ThomasFuston/dotemacs
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Commentary:
;;
;; This is the early-init file, it will be run before
;; init.el when Emacs is loaded.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Code:

;; Disable package initialization at startup
(setq package-enable-at-startup nil)

;; Increase the GC threshold for a faster startup
;; and increasing the allocation for Heap.
(setq gc-cons-threshold most-positive-fixnum
      gc-cons-percentage 0.6)

;; Unset file-name-handler
(defvar file-name-handler-alist-original file-name-handler-alist)
(setq file-name-handler-alist nil)

;; Disable GUI elements
(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)
(setq inhibit-splash-screen t)
(setq use-file-dialog nil)



(provide 'early-init)
;;; early-init.el ends here
