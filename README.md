# My personal Emacs config

This is my ever changing personal Emacs configuration, which is just
one of many such configurations but this one is mine. It has grown
and redone several times within the last 10 years.

Currently it is geared towards Webdevelopment and should work perfectly
on Linux and Emacs version 29+
